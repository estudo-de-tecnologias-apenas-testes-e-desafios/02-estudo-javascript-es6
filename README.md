# 02 - Estudo Javascript ES6

Estudo da Sintáxe, funcionalidades e possibilidades

no terminal faça yarn init

em seguida adicione @babel/cli -> faz com que possamos usar a interface de linha de comando do babel

adicione @babel/preset-env -> faz a conversão daquilo que o ambiente não entende ainda no caso os navegadores
Vai converter os comandos do EcmaScript 6 para comando que os navegadores mais antigos entendam

crie um arquivo chamado .babelrc o arquivo de configuração do babel e escreva:

abrindo um objeto
{
    "presets": ["@babel/preset-env"]
}

# package.json

adicione

"scripts": {
    "qualquer-nome" : [script->] "babel script-original -o[conversao] nome-do-arquivo-convertido -w" 
}

para rodar escreva no terminal yarn qualquer-nome

# -w é para fazer o processo de bundle automaticamente quando houver mudança, ou seja, não precisa ficar rodando o script toda a hora

# yarn add @babel/core para poder rodar a linha de comando criada 