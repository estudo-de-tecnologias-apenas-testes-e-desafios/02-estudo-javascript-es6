const verdade = true;

function EsperaJanta(a) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (a) resolve('A Janta está pronta');

        }, 5000);
    });
}

function EsperaTv(a) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (a) resolve('A TV está ligada');

        }, 1000);
    });
}

function SaiDoSofa(a) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (a) resolve('saiu do sofa');

        }, 1000);
    });
}

function Caminhando(a) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (a) resolve("Caminhando até a cozinha");
        }, 3000);
    });
}

console.log('Start');
console.log(EsperaJanta);
console.log(EsperaTv);

EsperaJanta(verdade)
    .then(mensagem => {
        console.log(mensagem);

        EsperaTv(verdade);
        SaiDoSofa(verdade);
        Caminhando(verdade);
    });
console.log("Depois do then EsperaJanta");

EsperaTv(verdade)
    .then(mensagem => console.log(mensagem));
console.log("Depois de EsperaTv");

SaiDoSofa(verdade)
    .then(mensagem => console.log(mensagem));
console.log("Depois de SaiDoSofa");

Caminhando(verdade)
    .then(mensagem => console.log(mensagem));
console.log("Depois de Caminhando");