function resolverDepoisDe2Segundos(x) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(x);
        }, 2000);
    });
}

function resolverDepoisDe5Segundos(x) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(x);
        }, 5000);
    });
}

async function adicionar1(x) {
    var a = resolverDepoisDe2Segundos(20);
    var b = resolverDepoisDe2Segundos(30);
    console.log("Start");
    console.log(await a);
    console.log(await b);
    console.log("End");
}

async function adicionar2(x) {
    console.log("Start");
    var a = await resolverDepoisDe2Segundos(20);
    var b = await resolverDepoisDe5Segundos(30);

    console.log(x);
    console.log(a);
    console.log(b);
    console.log("End");
}

var a, b;

a = () => {
    setTimeout(() => {
        let a = 1;
        console.log(a);
    }, 2000);
}

b = () => {
    setTimeout(() => {
        let b = 2;
        console.log(b);
    }, 1000);
}

a();
b();
console.log(a);
console.log(b);

//adicionar1(1); // a e b aparece depois de 2 segundos
//adicionar2(2); // a e b aparece depois de 4 segundos