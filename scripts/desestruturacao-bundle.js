"use strict";

var usuario = {
  nome: "Victor",
  idade: 19,
  endereco: {
    cidade: "Marilia",
    estado: "São Paulo"
  }
}; // Sem Desestruturação

var nomeWoutDesest = usuario.nome;
var idadeWoutDesest = usuario.idade;
var cidWoutDesest = usuario.endereco.cidade; // Com Desestruturação

var nome = usuario.nome,
    idade = usuario.idade,
    cidade = usuario.endereco.cidade;
console.log("nome: ".concat(nome, " \nidade:").concat(idade, " \nCidade: ").concat(cidade));

function mostraNome(_ref) {
  var nome = _ref.nome;
  return console.log("nome: ".concat(nome));
}

mostraNome(usuario);
