// REST
const usuario = {
    nome: "victor",
    idade: 19,
    cidade: "Marília"
}

const {nome, ...resto} = usuario;

console.log(nome,resto);

function soma(...params) {
    return params.reduce((total, next) => total + next);
}

console.log(soma(1,2,3));

//SPRED

const array1 = [1,2,3];
const array2 = [4,5,6];

const array3 = [...array1, ...array2];

console.log(array3);

const Victor = {
    nome: "Victor",
    idade: 19,
    cidade: "Marília"
}

const Jun = {...Victor, nome: "Jun", idade: 17};

console.log(Victor, Jun);