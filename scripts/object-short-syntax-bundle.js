"use strict";

var nome = "Emiya";
var idade = 19; // sem a sintaxe curta

var usuario = {
  nome: nome,
  idade: idade,
  classe: "Arqueiro"
}; // com a sintaxe curta

var heroi = {
  nome: nome,
  idade: idade,
  classe: "Archer"
};
console.log(usuario, heroi);
