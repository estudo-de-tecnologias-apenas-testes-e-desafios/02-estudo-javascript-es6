const usuario = {
    nome: "Victor",
    idade: 19,
    endereco: {
        cidade: "Marilia",
        estado: "São Paulo"
    }
}

// Sem Desestruturação
const nomeWoutDesest = usuario.nome;
const idadeWoutDesest = usuario.idade;
const cidWoutDesest = usuario.endereco.cidade;

// Com Desestruturação

const {nome, idade, endereco: {cidade}} = usuario;

console.log(`nome: ${nome} \nidade:${idade} \nCidade: ${cidade}`);

function mostraNome ({ nome }){
    return console.log(`nome: ${nome}`);
}

mostraNome(usuario);