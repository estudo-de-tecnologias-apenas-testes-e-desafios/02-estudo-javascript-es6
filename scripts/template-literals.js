const nome = "Victor";
const idade = 19;
const irmao = "Jun";

console.log("Meu nome é: "+ nome +" e tenho "+ idade +"\nTenho um irmao chamado: "+ irmao);

// Com o template Literals

console.log(`Com template Literals\nMeu nome é ${nome}, tenho ${idade} e tenho um irmao chamado ${irmao}`);