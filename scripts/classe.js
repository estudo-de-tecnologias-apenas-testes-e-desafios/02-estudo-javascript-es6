class Lista {
    constructor() {
        this.todos = [];
    }

    adiciona() {
        this.todos.push("novo");
        console.log(this.todos);
    }
}

class TudoList extends Lista {
    constructor() {
        super();// Chama o contructor da classe mae
        this.todos.push("Criada no constructor da classe filha");
        console.log(this.todos);
    }
}

class Matematica {

    static soma(a, b){
        return console.log(a + b);
    }
}

Matematica.soma(1, 6);


const botao = document.createElement("button");
botao.appendChild(document.createTextNode("Adicionar"));

document.querySelector("body").appendChild(botao);

const MinhaLista = new TudoList();

botao.onclick = () => {
    MinhaLista.adiciona();
}