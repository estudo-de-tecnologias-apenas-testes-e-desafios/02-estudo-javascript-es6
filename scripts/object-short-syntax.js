const nome = "Emiya";
const idade = 19;

// sem a sintaxe curta
const usuario = {
    nome: nome,
    idade: idade,
    classe: "Arqueiro"
}
// com a sintaxe curta

const heroi = {
    nome,
    idade,
    classe: "Archer"
}

console.log(usuario, heroi);
