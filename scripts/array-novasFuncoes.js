const lista = [1,5,4,2,6,3,8,7,9];

const novaLista = lista.map(function(valor, index) {
    return valor % 3 === 0 && index % 2 === 0;
});

console.log(novaLista);

const somaLista = lista.reduce(function(total, next) {
    return total + next;
});

console.log(somaLista);

const paresLista = lista.filter(function(valor) {
    return valor % 2 === 0;
});

console.log(paresLista);

const findLista = lista.find(function(valor) {
    return valor === 1;
});

if(findLista) console.log(findLista);

