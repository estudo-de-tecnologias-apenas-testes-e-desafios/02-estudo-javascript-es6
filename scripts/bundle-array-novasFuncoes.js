"use strict";

var lista = [1, 5, 4, 2, 6, 3, 8, 7, 9];
var novaLista = lista.map(function (valor, index) {
  return valor % 3 === 0 && index % 2 === 0;
});
console.log(novaLista);
var somaLista = lista.reduce(function (total, next) {
  return total + next;
});
console.log(somaLista);
var paresLista = lista.filter(function (valor) {
  return valor % 2 === 0;
});
console.log(paresLista);
var findLista = lista.find(function (valor) {
  return valor === 1;
});
if (findLista) console.log(findLista);
