document.querySelector("head title").innerHTML = "Exercicio 3 converta em arrow functions";

// 1

const arr = [1,2,3,4];

arr.map( item => item + 10);

// 2

const usuario = {nome: "Victor", idade: 19};

const mostraIdade = usuario => usuario.idade;

console.log(mostraIdade(usuario));

// 3

const nome = "Victor";
const idade = 19;

const mostraUsuario = (nome, idade) => ({nome, idade});

console.log(mostraUsuario(nome, idade));
console.log(mostraUsuario(nome));

// 4

const promise = () => new Promise( (resolve, reject) => resolve() );