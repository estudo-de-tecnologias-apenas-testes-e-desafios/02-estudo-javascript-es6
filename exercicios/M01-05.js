document.querySelector("head title").innerHTML = "Exericico 05 rest/spread";

const arr = [1,2,3,4,5,6];

const [x, ...y] = arr;

console.log(x);
console.log(y);

function soma (...param) {
    return param.reduce((total, next) => total + next);
}

console.log(soma(1,2,3,4,5,6));
console.log(soma(1,2));

// SPREAD

const usuario = {
    nome: "Victor",
    idade: 19,
    endereco: {
        cidade: "São Paulo",
        uf: "SP",
        pais: "Brasil"
    }
}

const usuario2 = {...usuario, nome: "Kratos"};
const usuario3 = {...usuario, cidade: "Londres"};