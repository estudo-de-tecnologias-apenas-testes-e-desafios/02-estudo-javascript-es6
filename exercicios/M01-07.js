document.querySelector("head title").innerHTML = "Exercicio 07 Object short syntax";

// apenas com nomes iguais as caracteristicas do objeto

const nome = "Victor";
const idade = 19;

const usuario = {
    nome,
    idade,
    cidade: "Ourinhos"
}

console.log(usuario);