document.querySelector("head title").innerHTML = "Exercicio 02 novos metodos array";

const usuarios = [
    {nome: "Diego", idade: 23, empresa: "Coronel Amido e filhos"},
    {nome: "Gabriel", idade: 15, empresa: "Coronel Amido e filhos"},
    {nome: "Lucas", idade: 30, empresa: "Chabas"}
];

const idades = usuarios.map( (valor) => valor.idade );
console.log(".map Mostre todas as idades");
console.log(idades);

const coronel = usuarios.filter( (valor) => valor.empresa === "Coronel Amido e filhos" && valor.idade > 18 );
console.log(".filter Mostre todas as pessoas que trabalham na Coronel Amido e filhos e que são maiores que 18 anos");
console.log(coronel);

const google = usuarios.find( valor => valor.empresa === "google" );
console.log(".find Todas as pessoas que trabalham no google");
console.log(google);

/*
const menor50 = usuarios.map(valor => {
    valor.idade *= 2;
    return valor;
});

console.log(usuarios);

APARENTEMENTE ISSO FAZ COM QUE ALTERE O VALOR DO OBJETO ORIGINAL USUARIOS E FAZ COM QUE O FILTER QUE É EXECUTADO PRIMEIRO SEJA ALTERADO, FAZENDO COM QUE OS 23 ANOS FIQUE COM 46 

**/

// ({...VALOR, IDADE: VALOR.IDADE * 2}) FAZ COM QUE CRIE UM NOVO OBJETO E NAO ALTERE USUARIOS
const menor50 = usuarios.map(valor => ({...valor, idade: valor.idade * 2})).filter(valor => valor.idade <= 50);
console.log("Mostre todas as pessoas que a idade * 2 e menor que 50");
console.log(menor50);
