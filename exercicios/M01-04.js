document.querySelector("head title").innerHTML = "exericicio 04 - Desestruturação";

const empresa = {
    nome: "Shin Ataraxia",
    endereco: {
        cidade: "São Paulo",
        estado: "SP"
    }
}

const {nome, endereco: {cidade, estado}} = empresa;

console.log(nome);
console.log(cidade);
console.log(estado);


const usuario = {
    nome: "Victor",
    idade: 19
}

function mostraInfo ({nome, idade}) {
    return `${nome} tem ${idade}`;
}

console.log(mostraInfo(usuario));